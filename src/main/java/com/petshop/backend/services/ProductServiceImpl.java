package com.petshop.backend.services;

import java.io.OutputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.WritableResource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.util.IOUtils;
import com.petshop.backend.models.Producto;
import com.petshop.backend.repositories.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	private ProductRepository productRepository;

	@Value("${aws.bucket.name}")
	private String bucket;

	private final ResourcePatternResolver resourcePatternResolver;
	private final ResourceLoader resourceLoader;
	private final AmazonS3 amazonS3;

	private final static String RUTA = "https://s3.us-east-2.amazonaws.com/bkcttest/";

	@Autowired
	public ProductServiceImpl(ResourcePatternResolver resourcePatternResolver, ResourceLoader resourceLoader,
			AmazonS3 amazonS3) {
		this.resourcePatternResolver = resourcePatternResolver;
		this.resourceLoader = resourceLoader;
		this.amazonS3 = amazonS3;
	}

	@Autowired
	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public List<Producto> getAllProducts() {
		return productRepository.findAll();
	}

	@Override
	public Producto getProductById(int id) {
		Producto producto = productRepository.findOne(id);
//		 MultipartFile file =
		return producto;
	}

	@Override
	public Producto saveProduct(Producto product) {
		try {

			List<Producto> productos = productRepository.findByOrderByIdDesc();

			MultipartFile file = product.getFile();

			Integer id = product.getId() == 0 ? productos.get(0).getId() + 1 : product.getId();

			String rutaFinal = RUTA + id + "/" + file.getOriginalFilename();

			String bucketPath = "s3://" + bucket + "/";

			Resource resource = this.resourceLoader.getResource(bucketPath + id + "/" + file.getOriginalFilename());

			System.out.println("Storing File :" + bucketPath + id + "/" + file.getOriginalFilename());

			WritableResource writableResource = (WritableResource) resource;

			try (OutputStream outputStream = writableResource.getOutputStream()) {
				byte[] bytes = IOUtils.toByteArray(file.getInputStream());

//				logger.info("Storing file {} in S3", file.getOriginalFilename());
				outputStream.write(bytes);

				product.setImagen(rutaFinal);
//				product.setImageBD(file.getOriginalFilename());
			} catch (Exception e) {
//				logger.error("Failed to upload file on S3");
				e.printStackTrace();
			}

			return productRepository.save(product);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

	@Override
	public void deleteProduct(Integer id) {
		productRepository.delete(id);
	}

	@Override
	public Page<Producto> findAll(Pageable pageable) {
		return productRepository.findAll(pageable);
	}
}
