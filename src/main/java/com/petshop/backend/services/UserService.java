package com.petshop.backend.services;

import com.petshop.backend.models.Usuario;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {
    List<Usuario> getAllUsers();

    List<Usuario> getAllExceptAdmin();

    Usuario getUserById(int id);

    Usuario saveUser(Usuario user);

    void deleteUser(Integer id);

    Page<Usuario> findAll(Pageable pageable);
}
