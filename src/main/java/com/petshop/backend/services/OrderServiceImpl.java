package com.petshop.backend.services;

import com.petshop.backend.models.Pedido;
import com.petshop.backend.repositories.OrderRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {
    private OrderRepository orderRepository;

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Pedido> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Pedido getOrderById(int id) {
        return orderRepository.findOne(id);
    }

    @Override
    public Pedido saveOrder(Pedido order) {
        return orderRepository.save(order);
    }

    @Override
    public void deleteOrder(Integer id) {
        orderRepository.delete(id);
    }

    @Override
    public Page<Pedido> findAll(Pageable pageable) {
        return orderRepository.findAll(pageable);
    }
}
