package com.petshop.backend.services;

import com.petshop.backend.models.Pedido;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderService {
    List<Pedido> getAllOrders();

    Pedido getOrderById(int id);

    Pedido saveOrder(Pedido order);

    void deleteOrder(Integer id);

    Page<Pedido> findAll(Pageable pageable);
}
