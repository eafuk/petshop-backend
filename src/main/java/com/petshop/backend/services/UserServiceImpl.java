package com.petshop.backend.services;

import com.petshop.backend.models.Usuario;
import com.petshop.backend.repositories.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<Usuario> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<Usuario> getAllExceptAdmin() {
        return userRepository.getAllExceptAdmin();
    }

    @Override
    public Usuario getUserById(int id) {
        return userRepository.findOne(id);
    }

    @Override
    public Usuario saveUser(Usuario user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.delete(id);
    }

    @Override
    public Page<Usuario> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }
}
