/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petshop.backend.services;

import com.petshop.backend.models.Producto;
import com.petshop.backend.models.Cita;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Antonio
 */
public interface QuoteService {
    
    Page<Cita> findAll(Pageable pageable);
}
