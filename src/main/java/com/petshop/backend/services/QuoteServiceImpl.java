/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petshop.backend.services;

import com.petshop.backend.models.Producto;
import com.petshop.backend.models.Cita;
import com.petshop.backend.repositories.QuoteRepository;
import com.petshop.backend.utils.UtilEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Antonio
 */
@Service
public class QuoteServiceImpl implements QuoteService {

	@Autowired
	private QuoteRepository quoteRepository;

	@Override
	public Page<Cita> findAll(Pageable pageable) {
		Page<Cita> citas = quoteRepository.findAll(pageable);

		for (int i = 0; i < citas.getContent().size(); i++) {
			if (citas.getContent().get(i).getServicioId() == UtilEnum.TipoCita.BAÑO.getId()) {
				citas.getContent().get(i).setType(UtilEnum.TipoCita.BAÑO.getDescripcion());
			} else if (citas.getContent().get(i).getServicioId() == UtilEnum.TipoCita.CORTE.getId()) {
				citas.getContent().get(i).setType(UtilEnum.TipoCita.CORTE.getDescripcion());
			}

			if (citas.getContent().get(i).getHorarioId() == UtilEnum.Horario.TEN.getId()) {
				citas.getContent().get(i).setHour_quote(UtilEnum.Horario.TEN.getDescripcion());
			} else if (citas.getContent().get(i).getHorarioId() == UtilEnum.Horario.ELEVEN.getId()) {
				citas.getContent().get(i).setHour_quote(UtilEnum.Horario.ELEVEN.getDescripcion());
			}
		}

		return citas;
	}

}
