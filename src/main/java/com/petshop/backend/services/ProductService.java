package com.petshop.backend.services;

import com.petshop.backend.models.Producto;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {
    List<Producto> getAllProducts();

    Producto getProductById(int id);

    Producto saveProduct(Producto product);

    void deleteProduct(Integer id);

    Page<Producto> findAll(Pageable pageable);
}
