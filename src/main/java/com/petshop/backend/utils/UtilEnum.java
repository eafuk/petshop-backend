package com.petshop.backend.utils;

public class UtilEnum {

	public enum TipoCita {

		BAÑO(1, "Baño"), CORTE(2, "Corte");

		private Integer id;
		private String descripcion;

		TipoCita(Integer id, String descripcion) {
			this.id = id;
			this.descripcion = descripcion;
		}

		public Integer getId() {
			return this.id;
		}

		public String getDescripcion() {
			return this.descripcion;
		}

	}
	
	public enum Horario {

		TEN(1, "10:00 a.m."), ELEVEN(2, "11:00 a.m.");

		private Integer id;
		private String descripcion;

		Horario(Integer id, String descripcion) {
			this.id = id;
			this.descripcion = descripcion;
		}

		public Integer getId() {
			return this.id;
		}

		public String getDescripcion() {
			return this.descripcion;
		}

	}
}
