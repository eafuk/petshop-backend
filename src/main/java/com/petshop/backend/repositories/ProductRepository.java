package com.petshop.backend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.petshop.backend.models.Producto;

public interface ProductRepository extends JpaRepository<Producto, Integer> {
	
	List<Producto> findByOrderByIdDesc();
}
