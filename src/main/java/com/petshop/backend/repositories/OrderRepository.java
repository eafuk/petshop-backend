package com.petshop.backend.repositories;

import com.petshop.backend.models.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Pedido, Integer> {
}
