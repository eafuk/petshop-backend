package com.petshop.backend.repositories;

import com.petshop.backend.models.Usuario;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<Usuario, Integer> {
    @Query(value = "SELECT *, CONCAT(nombres, ' ', apellidos) AS name FROM usuario WHERE id > 1", nativeQuery = true)
    List<Usuario> getAllExceptAdmin();
}
