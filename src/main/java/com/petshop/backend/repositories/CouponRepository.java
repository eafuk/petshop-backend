package com.petshop.backend.repositories;

import com.petshop.backend.models.Cupon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponRepository extends JpaRepository<Cupon, Integer> {
    Cupon getByCode(String code);
}
