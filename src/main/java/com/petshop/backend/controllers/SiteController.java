package com.petshop.backend.controllers;

import com.petshop.backend.models.Usuario;
import com.petshop.backend.services.OrderService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import com.petshop.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SiteController {
    private OrderService orderService;
    private UserService userService;
    protected EntityManager entityManager;

    @Autowired
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        // Getting total of sales
        StoredProcedureQuery storedProcedureQuery = this.entityManager.createStoredProcedureQuery("getTotalVentas");
        Double totalSales = storedProcedureQuery.getSingleResult() == null ? Double.valueOf("0") : Double.valueOf(storedProcedureQuery.getSingleResult().toString());
        // Getting the last users
        storedProcedureQuery = this.entityManager.createStoredProcedureQuery("getUltimoUsuario");

        storedProcedureQuery.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
        storedProcedureQuery.setParameter(1, 10);

        List<Object[]> rows = storedProcedureQuery.getResultList();
        ArrayList<Usuario> lastUsers = new ArrayList<>();

        for (Object[] row: rows) {
            lastUsers.add(this.userService.getUserById((int)row[0]));
        }

        modelAndView.addObject("totalSales", totalSales);
        modelAndView.addObject("lastUsers", lastUsers);
        modelAndView.setViewName("index");

        return modelAndView;
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied() {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("403");

        return modelAndView;
    }
}
