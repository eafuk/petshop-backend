package com.petshop.backend.controllers;

import com.petshop.backend.models.Usuario;
import com.petshop.backend.services.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRestController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/api/users", method = RequestMethod.GET)
    public List<Usuario> getAllEmployees() {
        return userService.getAllUsers();
    }

    @RequestMapping(value = "/api/users/{id}", method = RequestMethod.GET)
    public Usuario getEmployeeById(@PathVariable("id") int id) {
        return userService.getUserById(id);
    }
}
