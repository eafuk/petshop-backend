package com.petshop.backend.controllers;

import com.petshop.backend.models.Pedido;
import com.petshop.backend.models.Usuario;
import com.petshop.backend.services.OrderService;
import com.petshop.backend.services.UserService;
import com.petshop.backend.utils.PageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class OrderController {
    private OrderService orderService;
    private UserService userService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }
    
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String index(Model model, Pageable pageable) {
        Page<Pedido> orderPage = orderService.findAll(pageable);
        PageWrapper<Pedido> page = new PageWrapper<>(orderPage, "/orders");

        model.addAttribute("orders", page.getContent());
        model.addAttribute("page", page);

        return "order/index";
    }

    @RequestMapping("orders/create")
    public String newOrder(Model model) {
        model.addAttribute("order", new Pedido());
        model.addAttribute("action", "Agregar");

        return "order/form";
    }

    @RequestMapping("orders/update/{id}")
    public String update(@PathVariable Integer id, Model model) {
        model.addAttribute("order", orderService.getOrderById(id));
        model.addAttribute("action", "Editar");

        return "order/form";
    }

    @RequestMapping(value = "orders/save", method = RequestMethod.POST)
    public String save(Pedido order) {
        Pedido processedOrder = orderService.saveOrder(order);

        return "redirect:/coupons/vista/" + processedOrder.getId();
    }

    @GetMapping("orders/vista/{id}")
    public String view(@PathVariable Integer id, Model model) {
        
        Pedido pedido = orderService.getOrderById(id);
        model.addAttribute("order", pedido);

        return "order/vista";
    }

    @RequestMapping("order/delete/{id}")
    public String delete(@PathVariable Integer id){
        orderService.deleteOrder(id);

        return "redirect:/orders";
    }

    
}
