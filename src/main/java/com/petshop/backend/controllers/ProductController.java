package com.petshop.backend.controllers;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.petshop.backend.models.Producto;
import com.petshop.backend.services.ProductService;
import com.petshop.backend.utils.PageWrapper;

@Controller
public class ProductController {
	private ProductService productService;

	@Autowired
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public String index(Model model, Pageable pageable) {
		Page<Producto> productPage = productService.findAll(pageable);
		PageWrapper<Producto> page = new PageWrapper<>(productPage, "/products");

		model.addAttribute("products", page.getContent());
		model.addAttribute("page", page);

		return "product/index";
	}

	@RequestMapping("products/create")
	public String newProduct(Model model) {
		model.addAttribute("product", new Producto());
		model.addAttribute("action", "Agregar");

		return "product/form";
	}

	@RequestMapping("products/update/{id}")
	public String update(@PathVariable Integer id, Model model) {
		model.addAttribute("product", productService.getProductById(id));
		model.addAttribute("action", "Editar");

		return "product/form";
	}

	@RequestMapping(value = "products/save", method = RequestMethod.POST)
	public String save(Producto product) {
		Producto processedProduct = productService.saveProduct(product);

		return "redirect:/products/vista/" + processedProduct.getId();
	}

	@RequestMapping("products/vista/{id}")
	public String view(@PathVariable Integer id, Model model, HttpServletResponse response) {
		try {
			Producto producto = productService.getProductById(id);
			model.addAttribute("product", producto);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return "product/vista";
	}

	@RequestMapping("products/delete/{id}")
	public String delete(@PathVariable Integer id) {
		try {
			productService.deleteProduct(id);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return "redirect:/products";
	}

//	@RequestMapping("products/vista/{id}/img")
//	public String viewImg(@PathVariable Integer id, Model model, HttpServletResponse response) {
//		Producto producto = productService.getProductById(id);
//		try {
//
////			Blob blob = producto.getImageBD();
//
//			byte[] byteArray = blob.getBytes(1, (int) blob.length());
//			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
//			os.write(byteArray);
//			os.flush();
//			os.close();
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//		}
//
//		return "product/vista/" + producto.getId() + "/img";
//	}

	@RequestMapping("products/upload/{id}/img")
	public String updateImg(@RequestParam MultipartFile file) {

		System.out.println(file.getOriginalFilename());
		System.out.println(file);

		return "";
	}
}
