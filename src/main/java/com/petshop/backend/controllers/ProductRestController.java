package com.petshop.backend.controllers;

import com.petshop.backend.models.Producto;
import com.petshop.backend.services.ProductService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductRestController {
    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/api/products", method = RequestMethod.GET)
    public List<Producto> getAllEmployees() {
        return productService.getAllProducts();
    }

    @RequestMapping(value = "/api/products/{id}", method = RequestMethod.GET)
    public Producto getEmployeeById(@PathVariable("id") int id) {
        return productService.getProductById(id);
    }
}
