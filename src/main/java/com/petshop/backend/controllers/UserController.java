package com.petshop.backend.controllers;

import com.petshop.backend.models.Usuario;
import com.petshop.backend.services.UserService;
import com.petshop.backend.utils.PageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String index(Model model, Pageable pageable) {
        Page<Usuario> userPage = userService.findAll(pageable);
        PageWrapper<Usuario> page = new PageWrapper<>(userPage, "/users");

        model.addAttribute("users", page.getContent());
        model.addAttribute("page", page);

        return "user/index";
    }

    @RequestMapping("users/create")
    public String newUser(Model model) {
        model.addAttribute("user", new Usuario());
        model.addAttribute("action", "Agregar");

        return "user/form";
    }

    @RequestMapping("users/update/{id}")
    public String update(@PathVariable Integer id, Model model) {
        Usuario user = userService.getUserById(id);

        // If the user is not the admin
        if (user.getId() > 1) {
            model.addAttribute("user", user);
            model.addAttribute("action", "Editar");

            return "user/form";
        } else {
            return "redirect:/users";
        }
    }

    @RequestMapping(value = "users/save", method = RequestMethod.POST)
    public String save(Usuario user) {
        Usuario processedUser = userService.saveUser(user);

        return "redirect:/users/vista/" + processedUser.getId();
    }

    @RequestMapping("users/vista/{id}")
    public String view(@PathVariable Integer id, Model model) {
        model.addAttribute("user", userService.getUserById(id));

        return "user/vista";
    }

    @RequestMapping("user/delete/{id}")
    public String delete(@PathVariable Integer id){
        userService.deleteUser(id);

        return "redirect:/users";
    }
}
