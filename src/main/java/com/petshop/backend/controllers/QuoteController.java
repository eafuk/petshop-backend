/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petshop.backend.controllers;

import com.petshop.backend.models.Producto;
import com.petshop.backend.models.Cita;
import com.petshop.backend.models.Usuario;
import com.petshop.backend.services.QuoteService;
import com.petshop.backend.utils.PageWrapper;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Antonio
 */
@Controller
public class QuoteController {

    @Autowired
    private QuoteService quoteService;

    @RequestMapping(value = "/quotes", method = RequestMethod.GET)
    public String index(Model model, Pageable pageable) {
        Page<Cita> productPage = quoteService.findAll(pageable);
        PageWrapper<Cita> page = new PageWrapper<>(productPage, "/quotes");
        
        model.addAttribute("quotes", page.getContent());
        model.addAttribute("page", page);
        return "quote/index";
    }
}
