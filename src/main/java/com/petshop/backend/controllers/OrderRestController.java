package com.petshop.backend.controllers;

import com.petshop.backend.models.Pedido;
import com.petshop.backend.services.OrderService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderRestController {
    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/api/orders", method = RequestMethod.GET)
    public List<Pedido> getAllOrders() {
        return orderService.getAllOrders();
    }

    @RequestMapping(value = "/api/orders/{id}", method = RequestMethod.GET)
    public Pedido getEmployeeById(@PathVariable("id") int id) {
        return orderService.getOrderById(id);
    }
}
