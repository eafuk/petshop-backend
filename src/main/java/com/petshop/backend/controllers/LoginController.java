package com.petshop.backend.controllers;

import com.petshop.backend.models.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
	@RequestMapping(value={"/login"}, method = RequestMethod.GET)
	public ModelAndView showLoginPage(ModelAndView modelAndView, Usuario user){
        modelAndView.addObject("user", user);
        modelAndView.setViewName("logueo");

        return modelAndView;
	}
}
