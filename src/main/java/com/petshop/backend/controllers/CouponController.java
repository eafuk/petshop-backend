package com.petshop.backend.controllers;

import com.petshop.backend.models.Cupon;
import com.petshop.backend.models.Usuario;
import com.petshop.backend.services.CouponService;
import com.petshop.backend.services.UserService;
import com.petshop.backend.utils.PageWrapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CouponController {
    private CouponService couponService;
    private UserService userService;

    @Autowired
    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/coupons", method = RequestMethod.GET)
    public String index(Model model, Pageable pageable) {
        Page<Cupon> couponPage = couponService.findAll(pageable);
        PageWrapper<Cupon> page = new PageWrapper<>(couponPage, "/coupons");

        model.addAttribute("coupons", page.getContent());
        model.addAttribute("page", page);

        return "coupon/index";
    }

    @RequestMapping("coupons/create")
    public String newCoupon(Model model) {
        model.addAttribute("coupon", new Cupon());
        model.addAttribute("users", this.userService.getAllExceptAdmin());
        model.addAttribute("action", "Agregar");

        return "coupon/form";
    }

    @RequestMapping("coupons/update/{id}")
    public String update(@PathVariable Integer id, Model model) {
        Cupon cupon = couponService.getCouponById(id);
        List<Usuario> usuarios = this.userService.getAllExceptAdmin();
        model.addAttribute("coupon", cupon);
        model.addAttribute("users", usuarios);
        model.addAttribute("action", "Editar");

        return "coupon/form";
    }

    @RequestMapping(value = "coupons/save", method = RequestMethod.POST)
    public String save(Cupon coupon) {
        Cupon processedCoupon = couponService.saveCoupon(coupon);

        return "redirect:/coupons/vista/" + processedCoupon.getId();
    }

    @RequestMapping("coupons/vista/{id}")
    public String view(@PathVariable Integer id, Model model) {
        model.addAttribute("coupon", couponService.getCouponById(id));

        return "coupon/vista";
    }

    @RequestMapping("coupon/delete/{id}")
    public String delete(@PathVariable Integer id){
        couponService.deleteCoupon(id);

        return "redirect:/coupons";
    }
}
