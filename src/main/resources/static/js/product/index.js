$(document).ready(function () {
    var table = $('#products-table').DataTable({
        "sAjaxSource": "/api/products",
        "sAjaxDataProp": "",
        "order": [[0, "asc"]],
        "aoColumns": [
            {"mData": "id"},
            {"mData": "name"},
            {"mData": "price"}
        ]
    })
});
