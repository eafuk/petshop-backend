$(document).ready(function () {
    var table = $('#orders-table').DataTable({
        "sAjaxSource": "/api/orders",
        "sAjaxDataProp": "",
        "order": [[0, "asc"]],
        "aoColumns": [
            {"mData": "id"},
            {"mData": "firstName"},
            {"mData": "lastName"},
            {"mData": "email"},
            {"mData": "total"}
        ]
    })
});
