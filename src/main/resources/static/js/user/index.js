$(document).ready(function () {
    var table = $('#users-table').DataTable({
        "sAjaxSource": "/api/users",
        "sAjaxDataProp": "",
        "order": [[0, "asc"]],
        "aoColumns": [
            {"mData": "id"},
            {"mData": "firstName"},
            {"mData": "lastName"},
            {"mData": "email"}
        ]
    })
});
